var gulp = require('gulp'),
	clean = require('gulp-clean'),
	watch = require('gulp-watch'),
	plumber = require('gulp-plumber'),
	sass = require('gulp-sass'),
	csscomb = require('gulp-csscomb'),
	autoprefixer = require('gulp-autoprefixer'),
	connect = require('gulp-connect'),
	ejslocals = require('gulp-ejs-locals'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	prettify = require('gulp-prettify')

var fs = require('fs');

// Path
var src = './public/src';
var dist = './public/dist';

// Server
gulp.task('serve', function() {
	connect.server({
		root: dist,
		port: 8001,
		livereload: true,
		open: {
			browser: 'chrome'
		}
	});
});

//sass -> css
gulp.task('sass', function () {
	return gulp.src(src+'/scss/**/*.scss')
	.pipe( plumber({
		  errorHandler: function (error) {
			  console.log(error.message);
			  this.emit('end');
		  }
	  }) )
	.pipe(sass())
	.pipe(autoprefixer({
		browsers: ['last 3 versions','IE 8','android 2.3'],
		cascade: false
	}))
	.pipe(csscomb())
	.pipe(gulp.dest(dist+'/css'))
	.pipe(connect.reload())
});

// ejs -> HTML
gulp.task('ejs', function() {
	gulp
		.src([src+'/ejs/**/*.ejs',  '!' + src+'/ejs/**/_*.ejs'])
		.pipe(ejslocals(
			{jsonData: JSON.parse(fs.readFileSync(src+'/ejs/index.json'))},
			{ext: '.html'}
		))
		.pipe(prettify({indent_size: 1, indent_char:'\t'}))
		.pipe(plumber({
			handleError: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(gulp.dest(dist+'/html'))
		.pipe(connect.reload())
});

// js hint
gulp.task('js:hint', function () {
	gulp
		.src([src+'/js/*.js',  '!' + src+'/js/libs/*.js'])
		.pipe(gulp.dest(dist+'/js'))
		.pipe(connect.reload())
});

// image
gulp.task('imgmin', function() {
	gulp
		.src([src+'/images/**/*.{png,jpg,gif}'])
		// .pipe(imagemin({
		// 	progressive: true,
		//     interlaced:true,
		// 	use: [pngquant()]
		// }))
		.pipe(gulp.dest(dist+'/images'))
		.pipe(connect.reload())
});

// font
gulp.task('font', function () {
	gulp
		.src([src+'/fonts/**/*'])
		.pipe(gulp.dest(dist+'/fonts'))
});

// data
gulp.task('data', function () {
    gulp
        .src([src+'/data/**/*'])
        .pipe(gulp.dest(dist+'/data'))
});

// library
gulp.task('libs', function () {
	gulp
		.src([src+'/js/libs/**/*'])
		.pipe(gulp.dest(dist+'/js/libs/'))
		.pipe(connect.reload())
	gulp
		.src([src+'/css/libs/**/*'])
		.pipe(gulp.dest(dist+'/css/libs/'))
		.pipe(connect.reload())
});

// index
gulp.task('index', function () {
	gulp
		.src([src+'/index.html', src+'/list.html'])
		.pipe(gulp.dest(dist))
});

//clean 작업 설정

gulp.task('clean', function(){
	return gulp.src([dist+'/*',dist+'/*.html','!' + dist+'/.git', '!' + dist +'/*.md', '!' + dist +'/.gitignore'], {read: false})
		.pipe(clean());
});


// Watch task
gulp.task('watch',[], function() {
	watch(src+'/ejs/**/*.ejs', function() {
		gulp.start('ejs');
	});
	watch(src+'/scss/**/*.{scss,sass}', function() {
		gulp.start(['sass']);
	});
	watch([src+'/js/*.js',  '!' + src+'/js/libs/*.js'], function() {
		gulp.start('js:hint');
	});
	watch(src+'/images/**/*.{png,jpg,gif}', function() {
		gulp.start('imgmin');
	});
	watch([src+'/index.html', src+'/list.html'], function() {
		gulp.start('index');
	});
	watch([src+'/js/libs/**/*', src+'/css/libs/*'], function() {
		gulp.start('libs');
	});
});

gulp.task('default', ['serve','sass','ejs','js:hint','imgmin','watch','font','data','index','libs']);
