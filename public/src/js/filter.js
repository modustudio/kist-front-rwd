(function ($,win) {
    function checkFilter() {
        return function (e) {
            var $target = e.target || e.srcElement;
            var filterId = $($target).attr('data-filter');
            var $inputTarget = $('input[name='+filterId+']');
            for(var i=0; i<$inputTarget.length; i++){
                if($($inputTarget[i]).is(':disabled') && filterId === $($inputTarget[i]).attr('name')) {
                    $($inputTarget[i]).prop('disabled', false);
                } else {
                    $($inputTarget[i]).prop('disabled', true);
                    $($inputTarget[i]).prop('checked', false);
                }
            }
        }
    }

    window.addEventListener('DOMContentLoaded', function () {
        var $checkFilter = $('input[name^=filter]');
        var filterClickEvent = checkFilter();
        $checkFilter.on('change', filterClickEvent);
    });
})(jQuery, window);