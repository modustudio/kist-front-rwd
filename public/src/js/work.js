$(function () {
  var $datepicker = $( ".date-picker" );
  if($datepicker.length>0){
    $datepicker.datepicker();
  }

  $('.js-help-button').on('click', function () {
    if ($('body').attr('data-mobile') !== 'false') {
      if(!$(this).hasClass('active')){
        $('.js-help-button').removeClass('active');
        $(this).addClass('active');
      } else {
        $(this).removeClass('active');
      }
    }
  })
  $(document).on('touchend', function (e) {
    if ($('body').attr('data-mobile') !== 'false') {
      console.log($(e.target));
      if(!$(e.target).hasClass('js-help-button') && !$(e.target).prev().hasClass('js-help-button')){
        $('.js-help-button').removeClass('active');
      }
    }
  })
})

//layer popup
$(function () {
  var $popupInput = $("[data-popup]");
  var $popupCloseBtn = $("[data-popup-close]");

  $popupInput.on('change', function (event) {
    var $this = $(this);
    var element;
    if($this.attr('data-popup') && $this.is(":checked")){
      element = $this.attr('data-popup');
      layerPopup(element);
    }
  });

  $popupCloseBtn.on('click', function () {
    if($("body").attr("has-dimmed")){
      var element = $(this).attr('data-popup-close');
      $("body").removeAttr("has-dimmed");
      $('body').removeClass('is-not-scroll');
      $(".dimmed").fadeOut();
      $(element).fadeOut();
    }
  });

  function layerPopup(el) {
    if ($(el).length === 0) {
      alert("레이어팝업 없음");
      return false;
    }
    if(!$("body").attr("has-dimmed")){
      $("body").attr("has-dimmed",true);
      $('body').addClass('is-not-scroll');
      $(".dimmed").fadeIn();
      $(el).fadeIn();
    }
  }
});