$(function(){
    // Top Band Slider
    var $bandSliderBody = $('.js-band-banner-body');
    var bandSliderLen = $bandSliderBody.find('.js-band-slider > div').length;
    if(bandSliderLen === 0){
        $bandSliderBody.css("display","none");
    } else if(bandSliderLen > 1) {
        var stopAutoOption = true;

        /*var $this = $('.band-banner__inner');
        var $controls = $this.find('.bx-controls a');
        $controls.on('click mouseenter focus touchstart', function (e) {
            console.log(e.type);
            if(e.type === 'click' || 'mouseenter'){
                stopAutoOption = true;
            } else if(e.type === 'mouseleave'){
                stopAutoOption = false;
            } else {
                stopAutoOption = false;
            }
        });*/

        $('.js-band-slider').bxSlider({
            mode: 'vertical',
            pager: false,
            touchEnabled: false,
            auto: true,
            autoControls: false,
            autoHover: true,
            stopAutoOnClick: stopAutoOption,
            pause: 3000
        });
    }

    // Visual Slider
    var $mainSlider = $('.main-view__slide');
    var mainSliderLen = $('.main-view__slide li').length;
    if(mainSliderLen > 1){
        $mainSlider.removeClass('js-slide-none');
        $('.main-view__slide').bxSlider({

        });
    } else {
        $mainSlider.addClass('js-slide-none');
    }

    // Flex Slider
    var $flexSlider = $('.main-content__flex-container');
    var $flexSliderItem = $flexSlider.find('.main-content__flex-slide');
    if($flexSlider){
        $flexSliderItem.on("mouseenter", function (e) {
           var $this = $(this);
            $this.addClass('js-flex-on').siblings().removeClass('js-flex-on');
        });
    }

    // News List
    var $newsList = $('.flex-box.news .flex-box__list');
    var $newsListItem = $newsList.find('li');
    if($newsList){
        $newsListItem.on("click", function (e) {
            // e.preventDefault();
            var $this = $(this);
            $this.addClass('js-flex-on').siblings().removeClass('js-flex-on');
        });
    }

    // Info Slider
    var $infoSlider = $('.flex-box__info-slider .slide-banner');
    var infoSliderLen = $infoSlider.find('.slide-banner__item').length;
    if(infoSliderLen > 1){
        var infoSlider = $infoSlider.bxSlider({
            auto: true,
            autoControls: true,
            autoControlsCombine: true,
            autoHover: true,
            stopAutoOnClick: false,
            pager: false,
            onSliderLoad: function (currentIndex) {
                // console.log("slider Load");
                var cntHtml = '<div class="js-bx-counter"><span class="current-idx">'+(currentIndex+1)+'</span>&#47;<span class="total-cnt">'+infoSliderLen+'</span></div>';
                var $ctrls = $('.flex-box__info-slider .bx-controls');
                $ctrls.prepend(cntHtml);
            },
            onSlideBefore: function ($slideElement, oldIndex, newIndex) {
                var $currentItem = $infoSlider.closest('.bx-wrapper').find('.current-idx');
                var currentIdx = newIndex + 1;
                $currentItem.html(currentIdx);
            },
              onSliderResize: function (currentIndex) {
                infoSlider.reloadSlider();
              }
        });
    }

    // 보유장비 Slider
    var $accSlider = $('.acc-list__inner-list');
    $accSlider.each(function () {
        var accSliderLen = $(this).find('li:not(".bx-clone")').length;
        if(accSliderLen > 2){
            var accSlider = $(this).bxSlider({
                pager: false,
                minSlides: 2,
                maxSlides: 2,
                slideWidth: 5000,
                slideMargin: 0,
                moveSlides:2,
                infiniteLoop: false,
                onSliderLoad: function (currentIndex) {
                    // console.log("slider Load");
                    var accSliderLenPage = Math.ceil(accSliderLen/2);
                    var cntHtml = '<div class="js-bx-counter"><span class="current-idx">1</span>&#47;<span class="total-cnt">'+accSliderLenPage+'</span></div>';
                    var $ctrls = $(this).closest('li').find('.bx-controls');
                    $ctrls.prepend(cntHtml);
                },
                onSlideBefore: function ($slideElement, oldIndex, newIndex) {
                    var $currentItem = $(this).closest('li').find('.current-idx');
                    var currentIdx = newIndex + 1;
                    // console.log(currentIdx);
                    $currentItem.html(currentIdx);
                }
            });
        }
    });
    var $accItem = $('.acc-list__item');
    var accItemLen = $accItem.length;
    var accItemReminder = accItemLen % 3;
    if(accItemLen > 0){
        if(accItemReminder === 1){ //나머지 1일때
            $('.acc-list__item.nth-'+accItemLen).addClass('border-none');
        } else if(accItemReminder === 2){ //나머지 2일때
            // console.log(accItemReminder);
            $('.acc-list__item.nth-'+(accItemLen-1)).addClass('border-none');
            $('.acc-list__item.nth-'+accItemLen).addClass('border-none');
            $(window).resize(function() {
                if ($('body').attr('data-mobile') === 'false'){
                    $('.acc-list__item.nth-'+(accItemLen-1)).addClass('border-none');
                } else {
                    $('.acc-list__item.nth-'+(accItemLen-1)).removeClass('border-none');
                }
            });
        }
    }

    // $(window).resize(function(){
    //     if ($('body').attr('data-mobile') === 'false'){
    //         infoSlider.reloadSlider();
    //     }
    // });
});

/* 레이어팝업 */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function couponClose(){
    if($("input[name='chkbox']").is(":checked") ==true){
        setCookie("close","Y",1);
    }
    $(".popup_layer").hide();
}
$(document).ready(function(){
    cookiedata = document.cookie;
    if(cookiedata.indexOf("close=Y")<0){
        $(".popup_layer").show();
    }else{
        $(".popup_layer").hide();
    }
    $(".close").click(function(){
        couponClose();
    });
});

//dotdotdot
$(function () {
  $(window).on("resize", function() {
    var $target = $(".js-ellipsis");
    if($target.length > 0){
      $target.each(function () {
        $(this).dotdotdot({
          height: "watch",
          watch: "window"
        })
      });
    }
  }).trigger("resize");
});