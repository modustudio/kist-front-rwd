function loadScript(url, callback) {
  // Adding the script tag to the head as suggested before
  var head = document.getElementsByTagName('head')[0];
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = url;

  // Fire the loading
  head.appendChild(script);
}

loadScript("../../js/transition.height.js");

$(window).on("load resize", function () {
  $('body').attr('data-mobile',
    (function () {
      var r = ($(window).width() <= 1000) ? true : false;
      return r;
    }())
  );
}).resize();

$(function () {
  var wrapper = $('.wrapper');
  var logo = $('.top-logo');
  var gnb = $('#gnb');
  var gnbMap = $('#sitemap-btn');
  var siteMap = $('#sitemap');
  var siteMapBtn = $('.footer .sitemap__depth1-link');
  var gnbSearch = $('.gnb__search');
  var gnbSearchBtn = $('#gnb-search-btn');
  var gnbSearchClose = $('.gnb__search-close');
  var gnbState = 0;
  var gnbBg = '<div class="dimmed opa50">&nbsp;</div>';
  var utilBar = $('.util-bar');
  var bandBanner = $('.band-banner');
  var isGnb = false;
  var isGnbSearch = false;
  var isGnbBg = false;

  // iOS 키보드 닫기
  var hideKeyboard = function () {
    document.activeElement.blur();
    $("input").blur();
  };

  // 안드로이드 키보드 닫기


  function gnbBgBool() {
    // console.log("isGnbBg : ", isGnbBg, "isGnbSearch : ",isGnbSearch, "isGnb : ",isGnb);
    if (!isGnbSearch && !isGnb) {
      isGnbBg = false;
    }
    if (!isGnbBg && isGnbSearch || isGnb) {
      $('body').prepend(gnbBg);
      gnbBg = $('.dimmed.opa50');

      isGnbBg = true;
    } else {
      isGnbBg = false;
    }
    if ($('body').attr('data-mobile') === 'false') {
      $(gnbBg).css("top", 188);
    } else {
      $(gnbBg).css("top", 45);
    }
  }

  $(window).resize(function () {
    if ($('body').attr('data-mobile') === 'false') {
      $(gnb).removeClass('js-open-m').find("li").removeClass('js-open-m');
      $(logo).removeClass('js-open-m').find("li").removeClass('js-open-m');
      $(utilBar).removeClass('js-open-m');
      $(gnbMap).removeClass('js-open-m');
      $(bandBanner).removeClass('js-open-m');
      $(".gnb__depth1-item").css('height','inherit').attr('data-collapsed',true);
    } else {
      $(gnb).removeClass('js-open-d').find("li").removeClass('js-open-d');
      $(logo).removeClass('js-open-d').find("li").removeClass('js-open-d');
      $(gnbMap).removeClass('js-open-d');
      $(siteMap).removeClass('js-open-d');
      $('.js-open-map').each(function () {
        $(this).removeClass('js-open-map');
      });
      $(".gnb__depth1-item").css('height','55px');
    }

    gnbBgBool();
  });

  $(gnb.selector + ">ul>li>a").on("click mouseenter focus touchstart", function (e) {
    if ($('body').attr('data-mobile') === 'false') {
      if (!$(this).parents("li").hasClass("js-open-d")) {
        if (isGnbSearch === true) {
          $(gnbSearch).css("z-index", -2);
        }
        $(gnb).addClass('js-open-d');
        $(logo).addClass('js-open-d');
        $(this).parents("li").addClass("js-open-d").siblings("li").removeClass("js-open-d js-first");
        if (gnbState === 0) {
          $(this).parents("li").addClass("js-first");
          gnbState = 1;
        }

        isGnb = true;
        gnbBgBool();
      }
      /* else {
              $(gnb).removeClass('js-open-d');
              $(logo).removeClass('js-open-d');
              $(this).parents("li").removeClass("js-open-d");

                      gnbState = 0;
                      gnbBg.remove();
            }*/
      if (e.type == "touchstart") {
        return false;
      }
    } else {
      if (e.type === "click") {
        e.preventDefault();
        /*if ($(this).parents("li").hasClass("js-open-m")) {
         $(this).parents("li").removeClass("js-open-m");
        } else {
          $(this).parents("li").siblings().removeClass("js-open-m");
          $(this).parents("li").addClass("js-open-m");
        }*/
        var section = this.parentNode;
        var isCollapsed = section.getAttribute('data-collapsed') === 'true';
        // console.log("section:", section);
        // console.log("isCollapsed:", isCollapsed);
        if (!isCollapsed) {
          collapseSection(section, 55);
        } else {
          expandSection(section);
          section.setAttribute('data-collapsed', 'false');
        }
      }
    }

    if (isGnbSearch === false) {
      $(gnb).removeClass('js-open-search');
    }
  });
  $(gnb).on("mouseleave", function () {
    $(this).removeClass('js-open-d');
    $(logo).removeClass('js-open-d');
    //$(gnb).find("[class$=tit]").stop().animate({height:'0'},300);
    $(this).find('li').removeClass('js-open-d js-first');

    if (isGnbSearch === false) {
      $(gnbBg).remove();
      isGnbBg = false;
    } else {
      $(gnbSearch).css("z-index", 2000);
    }
    isGnb = false;
    gnbState = 0;
  });
  /*$(gnb).find("a").last().on("blur",function() {
    $(gnb).trigger("mouseleave");
  });*/

  // 전체메뉴, 검색
  $(gnb).find("[class$=sitemap-btn]").on("click", function () {
    if ($('body').attr('data-mobile') == "false") return false;
    if (!$(gnb).hasClass("js-open-m")) {
      $(gnb).addClass('js-open-m');
    } else {
      $(gnb).removeClass('js-open-m');
    }
  });
  $(gnbMap, gnbSearchBtn).on("mouseenter", function () {
    $(gnb).removeClass('js-open-d');
    $(logo).removeClass('js-open-d');
    $(gnb).find('li').removeClass('js-open-d js-first');
    if (isGnbSearch === false) {
      $(gnbBg).remove();
      isGnbBg = false;
    }
    gnbState = 0;
  });
  $(gnbMap).on("click touchstart", function (e) {
    if ($('body').attr('data-mobile') === 'false') {
      if (!$(this).hasClass("js-open-d")) {
          $(this).addClass('js-open-d');
          $(siteMap).addClass('js-open-d');
          $(gnb).addClass('js-open-map');
      } else {
          $(this).removeClass('js-open-d');
          $(siteMap).removeClass('js-open-d');
          $(gnb).removeClass('js-open-map');
      }
      if(e.type === "touchstart") {
          return false;
      }
    } else {
      if (e.type === "click") {
        if (!$(this).hasClass("js-open-m")) {
          if (isGnbSearch) {
            $(gnb).removeClass('js-open-search');
            $(gnbBg).remove();
            isGnbSearch = false;
            isGnbBg = false;
          }
          $(this).addClass('js-open-m');
          $(utilBar).addClass('js-open-m');
          $(bandBanner).addClass('js-open-m');
        } else {
          $(this).removeClass('js-open-m');
          $(utilBar).removeClass('js-open-m');
          $(bandBanner).removeClass('js-open-m');
        }
      }
    }
  });
  $(gnbSearchBtn.selector + '>a').on("click focus touchstart", function (e) {
    if (e.type === "click" || e.type === "touchstart") {
      if (!isGnbSearch) {
        $(gnb).addClass('js-open-search');

        isGnbSearch = true;
        gnbBgBool();
      }

      return false;
    }
  });
  $(gnbSearchClose.selector + '>a').on("click focus touchstart", function (e) {
    if (e.type === "click" || e.type === "touchstart") {
      if (!$('body').attr('data-mobile') === "false") hideKeyboard();
      if (isGnbSearch === true) {
        $(gnb).removeClass('js-open-search');
        $(gnbBg).remove();

        isGnbSearch = false;
        isGnbBg = false;
      }

      return false;
    }
  });
  // footer 사이트맵
  $(siteMapBtn).on("click focus touchstart", function (e) {
    if ($('body').attr('data-mobile') == 'false') return false;
    if (e.type == "click") {
      e.preventDefault();
      if ($(this).parents("li").hasClass("js-open-m")) {
        $(this).parents("li").removeClass("js-open-m");
      } else {
        $(this).parents("li").siblings().removeClass("js-open-m");
        $(this).parents("li").addClass("js-open-m");
      }
    }
  });

  $(window).on("scroll", function (e) {
    if($('body').find('.dimmed').length > 0){
      var dimmedTop = $('.gnb__search').offset().top - $(this).scrollTop();
      dimmedTop >= 0 ? $('.dimmed').css('top',dimmedTop) : $('.dimmed').css('top',0);
    }
    if ($('body').attr('data-mobile') === "false") return false;
    var wrap = $(gnb).parent();
    if ($(this).scrollTop() > $(wrap).height()) {
      $(wrap).addClass("js-fixed");
      $("#familySite").removeClass("js-open-m");
    } else {
      $(wrap).removeClass("js-fixed");
    }
  });
});

// page visual
(function (win) {
  function pageVisualSlider() {
    var $pageVisual = $('.page-visual__slider');
    var pageVisualLen = $pageVisual.find('.page-visual__slide-item:not(".bx-clone")').length;
    if ($pageVisual.length > 0 && pageVisualLen > 1) {
      var pageVisualSlider = $pageVisual.bxSlider({});
    }
  }

  function subLocationAnimate() {
    var $navLink = $('.location-nav__link');
    $navLink.on('click', function () {
      var $this = $(this);
      $this.next().hasClass('js-open') ? $this.next().removeClass('js-open') : $this.next().addClass('js-open');
    });
  }

  window.addEventListener('DOMContentLoaded', function () {
    subLocationAnimate();
  });

  window.addEventListener('load', function () {
    pageVisualSlider();
  });

  win.pageVisualSlider = pageVisualSlider;
})(window || {});

// top 배너
$(function () {
  var bandBtn = ".js-band-banner-btn";
  var bandBody = ".js-band-banner-body";
  var bandHeight = 70;

  if ($(bandBtn).length > 0) {
    $(bandBtn).on("click", function (e) {
      if ($('body').attr('data-mobile') === "false") {
        if ($(bandBody).hasClass("is-close")) {
          $(bandBody).removeClass("is-close").animate({
            "margin-top": "0px"
          }, 100, function () {
            $(bandBtn).toggleClass("is-flip");
          });
        } else {
          $(bandBody).addClass("is-close").animate({
            "margin-top": -(bandHeight) + "px"
          }, 100, function () {
            $(bandBtn).toggleClass("is-flip");
          });
        }
      } else {
        $(bandBody).removeClass('js-open-m');
      }
    });
  }
});

$(document).ready(function () {
  var familySite = $("#familySite");
  var jsOpenClass = '';
  $(familySite).find("[class$=tit]").on("click", function () {
    jsOpenClass = ($('body').attr('data-mobile') === "false") ? 'js-open-d' : 'js-open-m';
    if ($(familySite).hasClass(jsOpenClass)) {
      $(familySite).removeClass(jsOpenClass);
    } else {
      $(familySite).addClass(jsOpenClass);
    }
  });
  $(window).resize(function () {
    if ($('body').attr('data-mobile') == 'false') {
      $(familySite).removeClass("js-open-m");
    }
  });
});


//input:file
$(document).ready(function () {
  var fileTarget = $('.upload-hidden');

  fileTarget.on('change', function () {
    if (window.FileReader) {
      // 파일명 추출
      var filename = $(this)[0].files[0].name;
    } else {
      // Old IE 파일명 추출
      var filename = $(this).val().split('/').pop().split('\\').pop();
    }
    ;

    $(this).parent().siblings('.upload-name').val(filename);
  });

  //preview image
  var imgTarget = $('.upload-hidden');

  imgTarget.on('change', function () {
    var parent = $(this).parent().parent();
    parent.children('.upload-display').remove().end().removeClass('upload-set-v1_thumb');

    if (window.FileReader) {
      //image 파일만
      if (!$(this)[0].files[0].type.match(/image\//)) return;

      var reader = new FileReader();
      reader.onload = function (e) {
        var src = e.target.result;
        parent.prepend('<span class="upload-display"><img src="' + src + '" class="upload-thumb"></span>').addClass('upload-set-v1_thumb');
      }
      reader.readAsDataURL($(this)[0].files[0]);
    } else {
      //$(this)[0].select();
      //$(this)[0].blur();
      //var imgSrc = document.selection.createRange().text;
      //parent.prepend('<span class="upload-display"><img class="upload-thumb"></span>').addClass('upload-set-v1_thumb');

      //var img = $(this).parent().siblings('.upload-display').find('img');
      //img[0].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enable='true',sizingMethod='scale',src=\"" + imgSrc + "\")";
    }
  });

});

//tab
$(function () {
  var tab = $("[data-tab='true']");
  var hash = window.location.hash;
  var tt = [];

  $(tab).find("a").each(function (idx) {
    var t = $(tab).find("a").eq(idx).attr("href").split('#')[1];
    tt.push(t);

    if (hash) {
      sele($(tab).find("a[href=" + hash + "]"));
      for (var i in tt) {
        $("#" + tt[i]).removeClass("is-active");
      }
      $(hash).addClass("is-active");
    }

    $(this).on("click", function (e) {
      if (this.href.match(/#([^ ]*)/g)) {
        e.preventDefault();
        if (!$(this).parent().hasClass("is-active")) window.location.hash = ($(this).attr("href"));
        sele($(this));
        for (var i in tt) {
          $("#" + tt[i]).removeClass("is-active");
        }
        $("#" + t).addClass("is-active");

        if ($('#calendar').length > 0) {
          $('#calendar').fullCalendar('refetchEvents');
        }
      }
    });


  })
  if ($(tab).hasClass("tab-v1")) {
    $(tab).find("[class$=list]").on("click", function () {
      //console.log($('body').attr('data-mobile'));
      if ($('body').attr('data-mobile') == 'true') {
        if ($(this).hasClass("js-open-m")) {
          $(this).removeClass("js-open-m");
        } else {
          $(this).addClass("js-open-m");
        }
      }
    });
    $(window).resize(function () {
      if ($('body').attr('data-mobile') == 'false') $(".tab-v1").removeClass("js-open-m");
    });
  }

  function sele(el) {
    $(el).parent().addClass("is-active").siblings().removeClass("is-active");
  }

});

// 윈도우팝업
function windowOpen(url, w, h) {
  window.open(url, '_blank', 'toolbar=no, location=no, status=no, toolbar=no, menubar=no, width=' + w + ', height=' + h + ', scrollbars=yes, top=0, left=0');
}

//selectbox
$(function () {
  var selectBox = '.js-selectbox';
  if($(selectBox).length > 0){
    $(selectBox).each(function () {
      $(this).niceSelect();
    });
  }
})


//check all
// $(function(){
// 	var $checkAll = $('#checkAll');
//     var $inputFilter = $('input[name=filter]');
//     window.addEventListener('DOMContentLoaded', function () {
//         checkAllProp();
//         $checkAll.on('click', function () {
//             checkAllProp();
//         });
//         $inputFilter.on('click', function () {
//         	var $this = $(this);
//         	var thisChecked = $this.is(':checked') === false;
// 			if(thisChecked){
//                 $checkAll.prop('checked', false);
// 			}
//         });
//     });
//
//     function checkAllProp(){
//         if($checkAll.length > 0 && $inputFilter.length > 0){
// 			var checked = $checkAll.is(':checked');
// 			if(checked){
//                 $inputFilter.prop('checked', true);
// 			} else {
//                 $inputFilter.prop('checked', false);
// 			}
//         }
// 	}
// });
