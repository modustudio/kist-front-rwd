function collapseSection(element,elHeight) {
    element.style.height = (0+elHeight) + 'px';
    element.setAttribute('data-collapsed', 'true');
}

function expandSection(element) {
    var sectionHeight = element.scrollHeight;
    // console.log("expandSection :: sectionHeight:",sectionHeight);
    element.style.height = sectionHeight + 'px';
    element.setAttribute('data-collapsed', 'false');
}

function toggleAnimation(element,elHeight) {
    $(element).on('click', function(e) {
        var section = this.parentNode;
        var isCollapsed = section.getAttribute('data-collapsed') === 'true';
        // console.log("element:",element);
        // console.log("section:",section);
        // console.log("elHeight:",elHeight);
        // console.log("isCollapsed:",isCollapsed);
        if(!isCollapsed) {
            collapseSection(section,elHeight);
        } else {
            expandSection(section);
            section.setAttribute('data-collapsed', 'false');
        }
    });
}

function checkExpandHeight(element) {
    var item = $(element);
    item.each(function () {
        var isExpand = this.parentNode.getAttribute('data-collapsed') === 'false';
        var expandHeight = this.scrollHeight;
        if(isExpand){
          console.log(expandHeight);
          this.parentNode.style.height = expandHeight + 'px';
        }
    })
}